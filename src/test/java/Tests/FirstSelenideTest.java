package Tests;

import Pages.HomePage;

import Pages.HotelPage;
import Pages.SearchPage;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;


import java.time.Duration;
import java.util.Set;



public class FirstSelenideTest extends BaseTest{
    HomePage home = new HomePage();
    SearchPage spage = new SearchPage();
    HotelPage hotel = new HotelPage();
    /*public static String getCookys() {
        Set<Cookie> cookies = driver.manage().getCookies();
        return this;
    }*/
    //Set<Cookie> cookies = driver.manage().getCookies();
    //private static final String city = "Гамбург";

    @Test
    public void bookingHotel() {
        home.openHomePage();
        home.search("Лондон");
        spage.pokazatNaKarte();
        spage.getText();
        hotel.openSearchPage();
        hotel.assertHotel();
    }
}
