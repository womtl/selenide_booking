package Pages;

import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;


public class SearchPage {
    private final By POKAZAT_NA_KARTE = By.xpath("(//*[@data-testid='map-trigger'])[1]");
    private final By HOTEL = By.cssSelector("#b2searchresultsPage > div.map_full_overlay__wrapper > div.map_with_list__container > div.map_left_cards.map_left_cards--v2 > div.map_left_cards__container > div > a:nth-child(1) > div.map-card__content-container > div.map-card__title > span");
    private final By NAME_HOTEL = By.xpath("/html/body/div[12]/div[3]/div[2]/div[2]/div/a[1]/div[3]/div[1]/span");
    private final By NUMBER_STARS = By.xpath("/html/body/div[12]/div[3]/div[2]/div[2]/div/a[1]/div[3]/div[1]/span/span/span/span");
    private final By RATE_HOTEL = By.xpath("/html/body/div[12]/div[3]/div[2]/div[2]/div/a[1]/div[3]/div[2]/div/div/div[1]");
    private final By FEEDBACK_HOTEL = By.xpath("/html/body/div[12]/div[3]/div[2]/div[2]/div/a[1]/div[3]/div[2]/div/div/div[2]/div[2]");
    private final By COST_HOTEL = By.xpath("/html/body/div[12]/div[3]/div[2]/div[2]/div/a[1]/div[3]/div[3]/div[3]/div[2]/div/div[2]/span[1]");
    public String name;
    public String numberStars;
    public String rateHotel;
    public String feedbackHotel;
    public String costHotel;
    public SearchPage pokazatNaKarte() {
        $(POKAZAT_NA_KARTE).click();
        return this;
    }
    public SearchPage getText() {
        sleep(6000);
        $(HOTEL).hover();
        name=$(NAME_HOTEL).getText();
        numberStars=$(NUMBER_STARS).getText();
        rateHotel=$(RATE_HOTEL).getAttribute("aria-label");
        feedbackHotel=$(FEEDBACK_HOTEL).getText();
        costHotel=$(COST_HOTEL).getText();
        return this;
    }

}
