package Pages;

import Config.ProjectConfig;
import com.codeborne.selenide.Selenide;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;



public class HomePage {

    private static final ProjectConfig cfg = ConfigFactory.create(ProjectConfig.class);
    private final By COOKIES = By.xpath("//*[@id='onetrust-accept-btn-handler']");
    private final By LIST = By.xpath("(//*[@class='a80e7dc237'])[1]");
    private final By SEARCH_CITY = By.xpath("//*[@class='ce45093752']");
    private final By SEARCH_DATE = By.xpath("//*[@class='b91c144835']");
    private final By DATE = By.xpath("(//*[@class='e2f0d47913'])[38]");
    private final By SEARCH = By.xpath("//*[@class='fc63351294 a822bdf511 d4b6b7a9e7 cfb238afa1 c938084447 f4605622ad aa11d0d5cd']");


    public HomePage openHomePage() {
        Selenide.open(cfg.Url());
        sleep(300);
        return this;
    }
    public HomePage search(String city) {
        $(COOKIES).shouldBe(visible);
        $(COOKIES).click();
        $(SEARCH_CITY).clear();
        $(SEARCH_CITY).sendKeys(city);
        sleep(3000);
        $(SEARCH_DATE).click();
        $(DATE).shouldBe(visible);
        $(DATE).click();
        $(SEARCH).shouldBe(visible);
        $(SEARCH).click();
        return this;
    }

}
