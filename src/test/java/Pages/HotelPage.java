package Pages;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class HotelPage {
    SearchPage searchp = new SearchPage();
    private final By ASSERT_NAME = By.xpath("/html/body/div[3]/div/div[5]/div[1]/div[1]/div[1]/div/div[2]/div[11]/div[1]/div/div/h2");
    private final By ASSERT_NUMBER_STARS = By.xpath("//*[@data-testid='rating-stars']");
    private final By ASSERT_RATE = By.xpath("/html/body/div[3]/div/div[5]/div[1]/div[1]/div[1]/div/div[2]/div[13]/div/div/div[1]/div[2]/div/div[1]/a/div/div/div/div[1]");
    private final By ASSERT_FEEDBACK = By.xpath("/html/body/div[3]/div/div[5]/div[1]/div[1]/div[1]/div/div[2]/div[13]/div/div/div[1]/div[2]/div/div[1]/a/div/div/div/div[2]/div[2]");
    private final By ASSERT_COST = By.xpath("/html/body/div[3]/div/div[5]/div[1]/div[1]/div[3]/div/div[4]/div[3]/div/form/div[8]/div[1]/table/tbody/tr[1]/td[3]/div/div[1]/div[1]/div[2]/div/span[1]");
    private final By ATLAS_HOTEL = By.xpath("/html/body/div[12]/div[3]/div[4]/div[3]/div/div/div[2]/div[2]/div/div[3]/div[1]/div/div");
    public HotelPage openSearchPage() {
        $(ATLAS_HOTEL).click();
        sleep(3000);
        return this;
    }
    public HotelPage assertHotel() {
        $(ASSERT_NAME).shouldHave(text(searchp.name));
        $(ASSERT_NUMBER_STARS).shouldHave(text(searchp.numberStars));
        $(ASSERT_RATE).shouldHave(text(searchp.rateHotel));
        $(ASSERT_FEEDBACK).shouldHave(text(searchp.feedbackHotel));
        $(ASSERT_COST).shouldHave(text(searchp.costHotel));
        return this;
    }

}
